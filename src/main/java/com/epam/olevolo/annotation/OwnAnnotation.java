package com.epam.olevolo.annotation;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface OwnAnnotation {
    String value() default "default";
}
