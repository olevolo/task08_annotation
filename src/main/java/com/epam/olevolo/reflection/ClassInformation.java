package com.epam.olevolo.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class ClassInformation {

    public ClassInformation(Object o) {
        Class<?> clz = o.getClass();

        System.out.println("\nName: " + clz.getSimpleName() + "\nSuperclass: " + clz.getSuperclass());

        System.out.println("\nConstructors:");
        Constructor[] constructors = clz.getDeclaredConstructors();
        for (Constructor constructor : constructors) {
            System.out.println(constructor.getName() +", type of parameters: " + Arrays.toString(constructor.getParameterTypes()));
        }

        System.out.println("\nFields:");
        Field[] fields = clz.getDeclaredFields();
        for (Field field : fields
                ) {
            System.out.println("Name: " + field.getName() + ", type: " + field.getType());
        }

        System.out.println("\nMethods:");
        Method[] methods = clz.getDeclaredMethods();
        for (Method method : methods
                ) {
            System.out.println("Name: " + method.getName() + ", return type: " + method.getReturnType()
                    + " , parameters type: " + Arrays.toString(method.getParameterTypes()));
        }
    }
}