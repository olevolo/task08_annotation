package com.epam.olevolo.reflection;

import com.epam.olevolo.annotation.OwnAnnotation;

import java.lang.reflect.*;
import java.util.Arrays;
import java.util.List;

/**
 * @author Volodymyr Oleksiuk
 */

public class ReflectionDemo {
    public static int field1 = 1;
    private int field2 = 2;
    @OwnAnnotation
    private int field3 = 3;
    private int field4 = 4;
    @OwnAnnotation("abcd")
    private int field5 = 5;

    public static void main(String[] args) throws NoSuchFieldException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
        part1();
        System.out.println("***********************************");
        part2();
        System.out.println("***********************************");
        part3();
        System.out.println("***********************************");
        part4();
        System.out.println("***********************************");
        ClassInformation ci = new ClassInformation(new Pencil("pencil", "black", 1.4));
    }

    public void testMethod1() {
        System.out.println("I'm the easiest one");
    }

    public int testMethod2(int a, int b, int c) {
        return a + b + c;
    }

    public List<String> testMethod3(String args1, String args2) {
        return Arrays.asList(args1, args2);
    }

    public void testMethod4(String a, int ... args) {
        int sum = 0;
        for (int i = 0; i < args.length; i++)
            sum += args[i];
        System.out.println(a + " sum: " +  sum);
    }

    public void testMethod5(String ... a) {
        System.out.println(Arrays.toString(a));
    }

    public static void part1() {
        Field[] fields = ReflectionDemo.class.getDeclaredFields();
        for (Field f : fields) {
            if (f.isAnnotationPresent(OwnAnnotation.class))
                System.out.println(f.getType() + " " + f.getName());
        }
    }

    public static void part2() {
        Field[] fields = ReflectionDemo.class.getDeclaredFields();
        for (Field f : fields) {
            if (f.isAnnotationPresent(OwnAnnotation.class))
                System.out.println(f.getAnnotation(OwnAnnotation.class).value());
        }
    }

    public static void part3() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        ReflectionDemo obj = new ReflectionDemo();
        Method method1 = ReflectionDemo.class.getMethod("testMethod1");
        method1.invoke(obj, null);

        Method method2 = ReflectionDemo.class.getMethod("testMethod2", int.class, int.class, int.class);
        System.out.println(method2.invoke(obj, 3, 4, 2));

        Method method3 = ReflectionDemo.class.getMethod("testMethod3", String.class, String.class);
        System.out.println(method3.invoke(obj, "Yin", "Yan"));

        Method method4 = ReflectionDemo.class.getMethod("testMethod4", String.class, int[].class);
        method4.invoke(obj, "Yin", new int[]{3, 6, 7, 9});

        Method method5 = ReflectionDemo.class.getMethod("testMethod5", String[].class);
        String[] abc = new String[] {"abc", "def", "ghi"};
        method5.invoke(obj, new Object[]{abc});
    }

    public static void part4() throws NoSuchFieldException, IllegalAccessException, InstantiationException {
        ReflectionDemo own = new ReflectionDemo();
        Field field = ReflectionDemo.class.getField("field1");

        try {
            field.set(own, 5);
        } catch (IllegalArgumentException i) {
            System.out.println("Cannot!");
        }
        System.out.println(field1);
    }


}
