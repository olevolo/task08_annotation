package com.epam.olevolo.reflection;

public class Pencil {

    static final int LENGTH = 30;

    private String name;

    private String color;
    private double width;
    public Pencil(String name, String color, double width) {
        this.name = name;
        this.color = color;
        this.width = width;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }
}
